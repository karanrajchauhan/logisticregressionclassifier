%% LOAD DATA
tic
% load('data_SFcrime.mat');
load('OneHotVars.mat');

% load pre processed data 
X_ext = horzcat(HourOneHot, DaysOfWeekOneHot, DistrictsOneHot, ones(size(X,1), 1));
X_ext = X_ext';

% label vector
y = categorical(Category);

% unique classes (labels)
uniq_crime_types = categories(y);
num_crime_types = numel(uniq_crime_types);

% train test split
num_train = int32(0.6*size(X_ext,2));

X_ext_train = X_ext(:, 1:num_train);
X_ext_test = X_ext(:, num_train+1:end);

y_train = y(1:num_train);
y_test = y(num_train+1:end);

num_test = numel(y_test);

%% INITIALIZE

nll = 0;
lambda = 100;
eta = 10^-5;
max_iters = 500;

weights = zeros(size(X_ext_train,1), num_crime_types);

wtx_train = zeros(num_crime_types, num_train);
wtx_test = zeros(num_crime_types, num_test);

e_wtx_train = zeros(num_crime_types, num_train);
e_wtx_test = zeros(num_crime_types, num_test);

temp = zeros(num_crime_types, num_train);

grad_nll = zeros(size(X_ext_train,1), num_crime_types);
grad_f_theta = zeros(size(X_ext_train,1), num_crime_types);
f_theta = zeros(max_iters, 1);

logloss = zeros(max_iters, 1);

train_ccrs = zeros(max_iters, 1);
test_ccrs = zeros(max_iters, 1);

y_train_preds = zeros(1, num_train);
y_test_preds = zeros(1, num_test);

correct_train_preds = zeros(1, num_train);
correct_test_preds = zeros(1, num_test);

% if data point (col num j) belongs to class (col num i) then 1 else 0
is_pt_class_rownum = zeros(num_crime_types, num_train);
bool_vec = zeros(numel(y_train), 1);
for crime_idx = 1:num_crime_types
    bool_vec = y_train == uniq_crime_types(crime_idx);
    is_pt_class_rownum(crime_idx,:) = bool_vec';
end

is_test_pt_class_rownum = zeros(num_crime_types, num_test);
bool_vec = zeros(numel(y_test), 1);
for crime_idx = 1:num_crime_types
    bool_vec = y_test == uniq_crime_types(crime_idx);
    is_test_pt_class_rownum(crime_idx,:) = bool_vec';
end


%% TRAIN USING GRADIENT DESCENT

for i=1:max_iters
  
    % wtx for all points
    wtx_train = weights' * X_ext_train;
   
    % e^wtx for all x
    e_wtx_train = exp(wtx_train);
    
    % negative log likelihood
    nll = sum(log(sum(e_wtx_train, 1)), 2) - sum(sum(wtx_train .* is_pt_class_rownum));
    
    % f(theta)
    f_theta(i, 1) = nll + (0.5 * lambda * sum(sum(weights.^2, 1), 2));
    
    % take sum along each column, divide each num in that col by that sum
    temp = (e_wtx_train ./ sum(e_wtx_train, 1)) - is_pt_class_rownum;
    grad_nll = X_ext_train * temp';
    
    % add lambda * w vector for all classes
    grad_f_theta = grad_nll + lambda*weights;
    
    % updating weights
    weights = weights - eta*grad_f_theta;
    
    % calculate test and train ccrs
    [~, y_test_preds] = max(weights' * X_ext_test);
    [~, y_train_preds] = max(weights' * X_ext_train);

    y_test_preds = categorical(uniq_crime_types(y_test_preds))';
    y_train_preds = categorical(uniq_crime_types(y_train_preds))';

    correct_test_preds = y_test_preds(:)==y_test(:);
    correct_train_preds = y_train_preds(:)==y_train(:);

    test_ccrs(i, 1) = sum(correct_test_preds)/numel(correct_test_preds);
    train_ccrs(i, 1) = sum(correct_train_preds)/numel(correct_train_preds);
    
    % log loss
    wtx_test = weights' * X_ext_test;
    e_wtx_test = exp(wtx_test);
    
    logloss(i,1) = (sum(log(sum(e_wtx_test, 1)), 2) - sum(sum(wtx_test .* is_test_pt_class_rownum))) / num_test;
 
end

% save for later use some day
save('LogRegParams.mat', 'X_ext', 'y', 'num_train', 'weights', 'f_theta', 'y_train_preds', 'y_test_preds', 'train_ccrs', 'test_ccrs');

%% PLOTS

% f_theta against number of iterations
plot(1:500, f_theta);
ax = gca;
ax.Title.String = 'f(theta) vs Iterations';
ax.XLabel.String = 'Number of Iterations';
ax.YLabel.String = 'f(theta)';

figure;
% train ccr against number of iterations
plot(1:500, train_ccrs);
ax = gca;
ax.Title.String = 'Train CCRs vs Iterations';
ax.XLabel.String = 'Number of Iterations';
ax.YLabel.String = 'Train CCR';

figure;
% test ccr against number of iterations
plot(1:500, test_ccrs);
ax = gca;
ax.Title.String = 'Test CCRs vs Iterations';
ax.XLabel.String = 'Number of Iterations';
ax.YLabel.String = 'Test CCR';

figure;
% test ccr against number of iterations
plot(1:500, logloss);
ax = gca;
ax.Title.String = 'Log Loss vs Iterations';
ax.XLabel.String = 'Number of Iterations';
ax.YLabel.String = 'Log Loss';

figure;
% predicted values histogram
histogram(y_test_preds, uniq_crime_types);
ax = gca;
ax.Title.String = 'Crime Count vs Crime Category (predictions)';
ax.XLabel.String = 'Category';
ax.YLabel.String = 'Number of Crimes';

figure;
% actual values histogram
histogram(y_test, uniq_crime_types);
ax = gca;
ax.Title.String = 'Crime Count vs Crime Category (actual)';
ax.XLabel.String = 'Category';
ax.YLabel.String = 'Number of Crimes';

