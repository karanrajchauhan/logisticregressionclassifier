%load('data_SFcrime.mat');

tic
% extract hour from time
% hours = datetime(Dates).Hour;
hr_str = extractBetween(Dates, ' ', ':');
temp = sprintf('%s ', hr_str{:});
hours = sscanf(temp, '%f');

subplot(2,2,1);
histogram(hours, 0:24);

% set histogram properties
ax = gca;
ax.Title.String = 'Crime Count vs Crime Time';
ax.XLabel.String = 'Hour';
ax.YLabel.String = 'Number of Crimes';
ax.XLim = [0 23];
toc


tic
% get categorical array from cell
days = categorical(DayOfWeek);

subplot(2,2,2);
histogram(days, {'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'});

% set histogram properties
ax = gca;
ax.Title.String = 'Crime Count vs Crime Day';
ax.XLabel.String = 'Day of Week';
ax.YLabel.String = 'Number of Crimes';
toc


tic
% get categorical array from cell
districts = categorical(PdDistrict);

subplot(2,2,3);
histogram(districts, unique(districts));

% set histogram properties
ax = gca;
ax.Title.String = 'Crime Count vs Crime District';
ax.XLabel.String = 'District';
ax.YLabel.String = 'Number of Crimes';
toc


tic
% get categorical array from cell
categories = categorical(Category);

subplot(2,2,4);
histogram(categories, unique(categories));

% set histogram properties
ax = gca;
ax.Title.String = 'Crime Count vs Crime Category';
ax.XLabel.String = 'Category';
ax.YLabel.String = 'Number of Crimes';
toc
