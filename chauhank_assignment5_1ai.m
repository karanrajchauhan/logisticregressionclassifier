% load variables from provided dataset
load('data_SFcrime.mat');

% get total number of data points
num_pts = numel(DayOfWeek);

% num_pts x 7 matrix, each row is one hot rep of day of week
DaysOfWeekOneHot = zeros(num_pts, 7);

for idx = 1:num_pts
    
    day = DayOfWeek{idx};
    
    if strcmp(day, 'Sunday')
        DaysOfWeekOneHot(idx, 1) = 1;
    elseif strcmp(day, 'Monday')
        DaysOfWeekOneHot(idx, 2) = 1;
    elseif strcmp(day, 'Tuesday')
        DaysOfWeekOneHot(idx, 3) = 1;
    elseif strcmp(day, 'Wednesday')
        DaysOfWeekOneHot(idx, 4) = 1;
    elseif strcmp(day, 'Thursday')
        DaysOfWeekOneHot(idx, 5) = 1;
    elseif strcmp(day, 'Friday')
        DaysOfWeekOneHot(idx, 6) = 1;
    else
        DaysOfWeekOneHot(idx, 7) = 1;
    end
    
end

% extract just hour from time
% hours = datetime(Dates).Hour;
hr_str = extractBetween(Dates, ' ', ':');
temp = sprintf('%s ', hr_str{:});
hours = sscanf(temp, '%f');

% num_pts x 24 matrix, each row is one hot rep of hour of day (0 to 23)
HourOneHot = zeros(num_pts, 24);

for idx = 1:num_pts
    
    hour = hours(idx) + 1;
    HourOneHot(idx, hour) = 1;
    
end

% get unique districts, sort by order in which they appear in data
[districts, dist_idx] = (unique(PdDistrict));
[~, idx] = sort(dist_idx);
districts = districts(idx);

% num_pts x num_disticts matrix, each row is one hot rep of district
DistrictsOneHot = zeros(num_pts, numel(districts));

for idx = 1:num_pts
    
    distr = PdDistrict{idx};
    
    if strcmp(distr, 'NORTHERN')
        DistrictsOneHot(idx, 1) = 1;
    elseif strcmp(distr, 'PARK')
        DistrictsOneHot(idx, 2) = 1;
    elseif strcmp(distr, 'INGLESIDE')
        DistrictsOneHot(idx, 3) = 1;
    elseif strcmp(distr, 'BAYVIEW')
        DistrictsOneHot(idx, 4) = 1;
    elseif strcmp(distr, 'RICHMOND')
        DistrictsOneHot(idx, 5) = 1;
    elseif strcmp(distr, 'CENTRAL')
        DistrictsOneHot(idx, 6) = 1;
    elseif strcmp(distr, 'TARAVAL')
        DistrictsOneHot(idx, 7) = 1;
    elseif strcmp(distr, 'TENDERLOIN')
        DistrictsOneHot(idx, 8) = 1;
    elseif strcmp(distr, 'MISSION')
        DistrictsOneHot(idx, 9) = 1;
    else
        DistrictsOneHot(idx, 10) = 1;
    end
    
end

% concatenate all one hot vectors
X = horzcat(HourOneHot, DaysOfWeekOneHot, DistrictsOneHot);

% save the variables so that we won't have to load all data again
save('OneHotVars.mat', 'HourOneHot', 'DaysOfWeekOneHot', 'DistrictsOneHot');

