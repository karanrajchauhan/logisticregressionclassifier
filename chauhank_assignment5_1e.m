%load('data_SFcrime.mat');

% 37.7 to 37.82
% -122.54 to -122.32
% coordinates that are in mappable range
valid_coords =  (X(:,1) >= -122.54)...          % all satisfy
              & (X(:,1) <= -122.32)...          % 877982 satisfy
              & (Y(:,1) >= 37.3)...             % all satisfy
              & (Y(:,1) <= 37.82);              % 877982 satisfy


% map for drug/narcotic
dn_idx = strcmp(Category, 'DRUG/NARCOTIC') & valid_coords;

plot(X(dn_idx), Y(dn_idx), '.r', 'MarkerSize', 0.1);
plot_google_map


% % map for larceny/theft
% lt_idx = strcmp(Category, 'LARCENY/THEFT') & valid_coords;
% 
% plot(X(lt_idx), Y(lt_idx), '.b', 'MarkerSize', 0.01);
% plot_google_map
% 
% 
% % map for robbery
% r_idx = strcmp(Category, 'ROBBERY') & valid_coords;
%  
% plot(X(r_idx), Y(r_idx), '.k', 'MarkerSize', 0.1);
% plot_google_map




