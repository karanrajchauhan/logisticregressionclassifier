%load('data_SFcrime.mat');

% convert from cell to double array, extract hour from time
% hours = datetime(Dates).Hour;
hr_str = extractBetween(Dates, ' ', ':');
temp = sprintf('%s ', hr_str{:});
hours = sscanf(temp, '%f');

% convert from cell to categorical array
crime_types = categorical(Category);

% unique categories of crime
uniq_crime_types = categories(crime_types);

% most likely hour of crime for each crime
ml_hours = zeros(numel(uniq_crime_types), 1);

for type_idx = 1:numel(uniq_crime_types)
    
    % name of current category
    curr_crime_type = uniq_crime_types(type_idx);
    
    % indexing for data for current category
    x_idx = crime_types==curr_crime_type;
    
    % mode of hour for crimes in current category
    ml_hours(type_idx) = mode(hours(x_idx, 1));
    
    fprintf('Most likely hour for %s is %i\n', char(curr_crime_type), ml_hours(type_idx));
    
end

% cell to categorical array
pd_districts = categorical(PdDistrict);

% unique districts in dataset
uniq_districts = categories(pd_districts);

% most likely cateogory of crime for each district
ml_crime_types = zeros(numel(uniq_districts), 1);

for district_idx = 1:numel(uniq_districts)
    
    % name of current district
    district = uniq_districts(district_idx);
    
    % indexing for data for current district
    x_idx = pd_districts==district;
    
    % most likely crime in current district
    most_likely = mode(crime_types(x_idx, 1));
    
    % mode of hour for crimes in current district
    ml_crime_types(district_idx) = most_likely;
    
    fprintf('Most likely crime in %s is %s\n', char(district), char(most_likely));

end

