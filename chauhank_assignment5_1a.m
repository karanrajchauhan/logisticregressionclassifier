%% LOAD AND PREPROCESS DATA

% load variables from provided dataset
load('data_SFcrime.mat');

% get total number of data points
num_pts = numel(DayOfWeek);

% num_pts x 7 matrix, each row is one hot rep of day of week
DaysOfWeekOneHot = zeros(num_pts, 7);

for idx = 1:num_pts
    
    day = DayOfWeek{idx};
    
    if strcmp(day, 'Sunday')
        DaysOfWeekOneHot(idx, 1) = 1;
    elseif strcmp(day, 'Monday')
        DaysOfWeekOneHot(idx, 2) = 1;
    elseif strcmp(day, 'Tuesday')
        DaysOfWeekOneHot(idx, 3) = 1;
    elseif strcmp(day, 'Wednesday')
        DaysOfWeekOneHot(idx, 4) = 1;
    elseif strcmp(day, 'Thursday')
        DaysOfWeekOneHot(idx, 5) = 1;
    elseif strcmp(day, 'Friday')
        DaysOfWeekOneHot(idx, 6) = 1;
    else
        DaysOfWeekOneHot(idx, 7) = 1;
    end
    
end

% extract just hour from time
hr_str = extractBetween(Dates, ' ', ':');
temp = sprintf('%s ', hr_str{:});
hours = sscanf(temp, '%f');

% num_pts x 24 matrix, each row is one hot rep of hour of day (0 to 23)
HourOneHot = zeros(num_pts, 24);

for idx = 1:num_pts
    
    hour = hours(idx) + 1;
    HourOneHot(idx, hour) = 1;
    
end

% get unique districts, sort by order in which they appear in data
[districts, dist_idx] = (unique(PdDistrict));
[~, idx] = sort(dist_idx);
districts = districts(idx);

% num_pts x num_disticts matrix, each row is one hot rep of district
DistrictsOneHot = zeros(num_pts, numel(districts));

for idx = 1:num_pts
    
    distr = PdDistrict{idx};
    
    if strcmp(distr, 'NORTHERN')
        DistrictsOneHot(idx, 1) = 1;
    elseif strcmp(distr, 'PARK')
        DistrictsOneHot(idx, 2) = 1;
    elseif strcmp(distr, 'INGLESIDE')
        DistrictsOneHot(idx, 3) = 1;
    elseif strcmp(distr, 'BAYVIEW')
        DistrictsOneHot(idx, 4) = 1;
    elseif strcmp(distr, 'RICHMOND')
        DistrictsOneHot(idx, 5) = 1;
    elseif strcmp(distr, 'CENTRAL')
        DistrictsOneHot(idx, 6) = 1;
    elseif strcmp(distr, 'TARAVAL')
        DistrictsOneHot(idx, 7) = 1;
    elseif strcmp(distr, 'TENDERLOIN')
        DistrictsOneHot(idx, 8) = 1;
    elseif strcmp(distr, 'MISSION')
        DistrictsOneHot(idx, 9) = 1;
    else
        DistrictsOneHot(idx, 10) = 1;
    end
    
end

% concatenate all one hot vectors
X = horzcat(HourOneHot, DaysOfWeekOneHot, DistrictsOneHot);

% save the variables so that we won't have to load all data again
save('OneHotVars.mat', 'HourOneHot', 'DaysOfWeekOneHot', 'DistrictsOneHot');


%% CREATE PLOTS

% hours histogram
histogram(hours, 0:24);

% set histogram properties
ax = gca;
ax.Title.String = 'Crime Count vs Crime Time';
ax.XLabel.String = 'Hour';
ax.YLabel.String = 'Number of Crimes';
ax.XLim = [0 23];
figure;

% get categorical array from cell
days = categorical(DayOfWeek);

histogram(days, {'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'});

% set histogram properties
ax = gca;
ax.Title.String = 'Crime Count vs Crime Day';
ax.XLabel.String = 'Day of Week';
ax.YLabel.String = 'Number of Crimes';
figure;

% get categorical array from cell
districts = categorical(PdDistrict);

histogram(districts, unique(districts));

% set histogram properties
ax = gca;
ax.Title.String = 'Crime Count vs Crime District';
ax.XLabel.String = 'District';
ax.YLabel.String = 'Number of Crimes';
figure;

% get categorical array from cell
crime_types = categorical(Category);

% unique categories of crime
uniq_crime_types = categories(crime_types);

histogram(crime_types, uniq_crime_types);

% set histogram properties
ax = gca;
ax.Title.String = 'Crime Count vs Crime Category';
ax.XLabel.String = 'Category';
ax.YLabel.String = 'Number of Crimes';


%% MOST LIKELY HOURS AND CRIMES

% most likely hour of crime for each crime
ml_hours = zeros(numel(uniq_crime_types), 1);

for type_idx = 1:numel(uniq_crime_types)
    
    % name of current category
    curr_crime_type = uniq_crime_types(type_idx);
    
    % indexing for data for current category
    x_idx = crime_types==curr_crime_type;
    
    % mode of hour for crimes in current category
    ml_hours(type_idx) = mode(hours(x_idx, 1));
    
    fprintf('Most likely hour for %s is %i\n', char(curr_crime_type), ml_hours(type_idx));
    
end

% cell to categorical array
pd_districts = categorical(PdDistrict);

% unique districts in dataset
uniq_districts = categories(pd_districts);

% most likely cateogory of crime for each district
ml_crime_types = zeros(numel(uniq_districts), 1);

for district_idx = 1:numel(uniq_districts)
    
    % name of current district
    district = uniq_districts(district_idx);
    
    % indexing for data for current district
    x_idx = pd_districts==district;
    
    % most likely crime in current district
    most_likely = mode(crime_types(x_idx, 1));
    
    % mode of hour for crimes in current district
    ml_crime_types(district_idx) = most_likely;
    
    fprintf('Most likely crime in %s is %s\n', char(district), char(most_likely));

end


%% GOOGLE MAPS VISUALIZATION

load('data_SFcrime.mat','X');
load('data_SFcrime.mat','Y');

% 37.7 to 37.82
% -122.54 to -122.32
% coordinates that are in mappable range
valid_coords =  (X(:,1) >= -122.54)...          % all satisfy
              & (X(:,1) <= -122.32)...          % 877982 satisfy
              & (Y(:,1) >= 37.3)...             % all satisfy
              & (Y(:,1) <= 37.82);              % 877982 satisfy

% map for drug/narcotic
dn_idx = strcmp(Category, 'DRUG/NARCOTIC') & valid_coords;

plot(X(dn_idx), Y(dn_idx), '.r', 'MarkerSize', 0.1);
plot_google_map
figure;

% map for larceny/theft
lt_idx = strcmp(Category, 'LARCENY/THEFT') & valid_coords;

plot(X(lt_idx), Y(lt_idx), '.b', 'MarkerSize', 0.01);
plot_google_map
figure;

% map for robbery
r_idx = strcmp(Category, 'ROBBERY') & valid_coords;
 
plot(X(r_idx), Y(r_idx), '.k', 'MarkerSize', 0.1);
plot_google_map

